import java.util.Scanner;

public class SimpleCalculator{
    public static void main(String[] args){
        int operation;
        int num1;
        int num2;
        int ans = 0;
        Scanner inputReader = new Scanner(System.in);

        System.out.println("Welcome to the Simple Calculator.");

        do{
            System.out.println("Enter 1 for addition.");
            System.out.println("Enter 2 for subtraction.");
            System.out.println("Enter 3 for multiplication.");
            System.out.println("Enter 4 for division.");
            System.out.println("Enter 5 for remainder.");
            System.out.println("Enter 0 to quit.");
            System.out.print("Enter choice here: ");
            operation = inputReader.nextInt();

            if(operation <= 5 && operation > 0){
                System.out.print("Enter first number: ");
                num1 = inputReader.nextInt();
    
                System.out.print("Enter second number: ");
                num2 = inputReader.nextInt();

                if(operation == 1){
                    ans = num1 + num2;
                } else if(operation == 2){
                    ans = num1 - num2;
                } else if(operation == 3){
                    ans = num1 * num2;
                } else if(operation == 4){
                    ans = num1 / num2;
                } else if(operation == 5){
                    ans = num1 % num2;
                }

                System.out.println("the answer is: " + ans);

            } else if (operation == 0) {
                System.out.println("Bye");
            } else {
                System.out.println("Try again and enter CORRECT operation");
            }
        }while (operation != 0); // end of the while loop
    } // end of the main method
} // end of the program
