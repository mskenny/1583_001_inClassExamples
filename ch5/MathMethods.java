public class MathMethods{

    public static void main(String[] args){
        int num = max(21,36,40);
        System.out.println(num);
    }

    public static int max(int a, int b, int c){
        int maximum = a;
        if(b > maximum){
            maximum = b;
        }
        if(c > maximum){
            maximum = c;
        }
        return maximum;
    }

}
